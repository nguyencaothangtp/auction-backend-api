<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        /*
        |--------------------------------------------------------------------------
        | Permission for users table
        |--------------------------------------------------------------------------
        */
        $permission = Permission::create(['name' => 'get users']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'list users']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'create users']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'update users']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'delete users']);
        $permission->syncRoles([
            'Admin',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Permission for roles table
        |--------------------------------------------------------------------------
        */
        $permission = Permission::create(['name' => 'get roles']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'list roles']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'create roles']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'update roles']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'delete roles']);
        $permission->syncRoles([
            'Admin',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Permission for permissions table
        |--------------------------------------------------------------------------
        */
        $permission = Permission::create(['name' => 'get permissions']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'list permissions']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'create permissions']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'update permissions']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'delete permissions']);
        $permission->syncRoles([
            'Admin',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Permission for products table
        |--------------------------------------------------------------------------
        */
        $permission = Permission::create(['name' => 'get products']);
        $permission->syncRoles([
            'Admin',
            'Regular'
        ]);

        $permission = Permission::create(['name' => 'list products']);
        $permission->syncRoles([
            'Admin',
            'Regular'
        ]);

        $permission = Permission::create(['name' => 'create products']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'update products']);
        $permission->syncRoles([
            'Admin',
        ]);

        $permission = Permission::create(['name' => 'delete products']);
        $permission->syncRoles([
            'Admin',
        ]);
    }
}
