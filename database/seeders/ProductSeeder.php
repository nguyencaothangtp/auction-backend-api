<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'iPhone 11 Pro Max',
            'description' => 'Apple iPhone 11 Pro Max 256GB',
            'img_url' => 'https://fptshop.com.vn/Uploads/images/2015/Tin-Tuc/QuanLNH2/iphone-11-pro-max-5.jpg',
            'initial_price' => '1000',
            'status' => Product::STATUS_OPEN,
        ]);

        Product::create([
            'name' => 'iPhone XS Max',
            'description' => 'Apple iPhone XS Max 512',
            'img_url' => 'https://vnn-imgs-f.vgcloud.vn/2019/09/12/15/iphone-xs-va-xs-max-giam-gia-sau-apple-don-duong-ban-iphone-11.jpg',
            'initial_price' => '800',
            'status' => Product::STATUS_OPEN,
        ]);

        Product::create([
            'name' => 'Samsung Galaxy S10',
            'description' => 'Samsung Galaxy S10 brand new',
            'img_url' => 'https://fptshop.com.vn/uploads/images/tin-tuc/113370/Originals/samsung-galaxy-s10-white-7.jpg',
            'initial_price' => '900',
            'status' => Product::STATUS_OPEN,
        ]);

        Product::create([
            'name' => 'Macbook Pro 2020',
            'description' => 'Apple Macbook Pro 2020',
            'img_url' => 'https://mecongnghe.vn/wp-content/uploads/2021/01/apple-macbook-pro-m1-2020-5.jpg',
            'initial_price' => '1200',
            'status' => Product::STATUS_OPEN,
        ]);


        Product::create([
            'name' => 'Filco Mechanical Keyboard',
            'description' => 'Keyboard filco majestouch convertible 2 matcha latte 87',
            'img_url' => 'https://www.phongcachxanh.vn/web/image/product.product/10747/image_1024',
            'initial_price' => '300',
            'status' => Product::STATUS_OPEN,
        ]);

    }
}
