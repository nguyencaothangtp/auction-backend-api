<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = User::create([
            'username' => 'admin',
            'full_name' => 'Administrator',
            'phone_number' => '0123456789',
            'password' => Hash::make('123456'),
            'is_active' => true
        ]);
        $user->assignRole(['Admin']);

        $user = User::create([
            'username' => 'user1',
            'full_name' => 'Regular User 1',
            'phone_number' => '0987654321',
            'password' => Hash::make('123456'),
            'maximum_bid_amount' => 5000,
            'is_active' => true
        ]);
        $user->assignRole(['Regular']);

        $user = User::create([
            'username' => 'user2',
            'full_name' => 'Regular User 2',
            'phone_number' => '0987654322',
            'password' => Hash::make('123456'),
            'maximum_bid_amount' => 2000,
            'is_active' => true
        ]);
        $user->assignRole(['Regular']);
    }
}
