<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique()->nullable();
            $table->string('full_name', 100);
            $table->string('phone_number', 15);
            $table->string('password', 250);
            $table->string('api_token', 250)->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('confirmation_token', 250)->nullable();
            $table->string('reset_token', 250)->nullable();
            $table->dateTime('reset_sent_at')->nullable();
            $table->rememberToken();

            $table->unsignedDouble('maximum_bid_amount')->default(0);
            $table->unsignedDouble('current_bid_amount')->default(0);

            $table->boolean('is_active')->default(false);
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
