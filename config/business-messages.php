<?php

return [
    'system_exceptions' => [
        'unknown_error' => [
            'message_code' => "UNKNOWN_ERROR",
            'message' => "Unknown error."
        ],
        'unauthorized' => [
            'message_code' => "UNAUTHORIZED",
            'message' => "Unauthorized"
        ],
        'missing_params' => [
            'message_code' => "MISSING_PARAMS",
            'message' => "There are missing parameters."
        ],
        'resource_not_found' => [
            'message_code' => "RESOURCE_NOT_FOUND",
            'message' => "Resource not found."
        ],
        'invalid_params' => [
            'message_code' => "INVALID_PARAMS",
            'message' => "There are invalid parameters."
        ],
    ],

    'account' => [
        'messages' => [
            'invalid_password' => [
                'message_code' => "INVALID_PASSWORD",
                'message' => "Invalid password"
            ],
            'not_found' => [
                'message_code' => "ACCOUNT_NOT_FOUND",
                'message' => "Account not found"
            ],
            'token_not_found' => [
                'message_code' => "TOKEN_NOT_FOUND",
                'message' => "Token not found"
            ],
            'user_not_found' => [
                'message_code' => "USER_NOT_FOUND",
                'message' => "User not found"
            ],
            'token_expired' => [
                'message_code' => "TOKEN_EXPIRED",
                'message' => "Token is expired"
            ],
            'email_send_failed' => [
                'message_code' => "EMAIL_SEND_FAILED",
                'message' => "Failed to send email"
            ]
        ]
    ],

    'permission' => [
        'messages' => [
            'existed' => [
                'message_code' => "PERMISSION_EXISTED",
                'message' => "Permission is already existed"
            ],
        ]
    ],

    'role' => [
        'messages' => [
            'existed' => [
                'message_code' => "ROLE_EXISTED",
                'message' => "Role is already existed"
            ],
        ]
    ],

    'product' => [
        'messages' => [
            'not_found' => [
                'message_code' => "PRODUCT_NOT_FOUND",
                'message' => "Product not found"
            ],
        ]
    ],

    'bidding' => [
        'messages' => [
            'exceed_maximum_funds' => [
                'message_code' => "EXCEEDED_MAXIMUM_FUNDS",
                'message' => "Exceeded maximum funds"
            ],
            'bid_closed' => [
                'message_code' => "BID_WAS_CLOSED",
                'message' => "Bid's already closed"
            ],
            'latest_bid' => [
                'message_code' => "LATEST_BID",
                'message' => "You are the latest bidder for this product"
            ],
            'invalid_bidding_amount' => [
                'message_code' => "INVALID_BIDDING_AMOUNT",
                'message' => "Invalid bidding amount"
            ],
        ]
    ]
];
