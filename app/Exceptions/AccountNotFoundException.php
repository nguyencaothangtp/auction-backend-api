<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class AccountNotFoundException extends InvalidArgumentException
{
    public $businessmessage;

    /**
     * Constructor
     */
    public function __construct($message = null)
    {
        if ($message) {
            $this->businessmessage = $message;
        } else {
            $config = config('business-messages');
            $this->businessmessage = $config['account']['messages']['not_found'];
        }
    }

    public function report()
    {
        $exceptionMessage = $this->businessmessage;
        Log::warning($exceptionMessage);
    }

    public function render()
    {
        // Sending 400 in API responses
        $payload = $this->businessmessage;

        return response()->json($payload, Response::HTTP_BAD_REQUEST);
    }
}
