<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class SystemException extends InvalidArgumentException
{
    public $businessmessage;

    /**
     * Constructor
     */
    public function __construct($message = null)
    {
        if ($message) {
            $this->businessmessage = $message;
        } else {
            $config = config('business-messages');
            $this->businessmessage = $config['system_exceptions']['unknown_error'];
        }
    }

    public function report()
    {
        $exceptionMessage = $this->businessmessage;
        Log::error($exceptionMessage);
    }

    public function render()
    {
        // Sending 500 in API responses
        $payload = $this->businessmessage;

        return response()->json($payload, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
