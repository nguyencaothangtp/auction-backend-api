<?php

namespace App\Providers;

use App\Repositories\AutoBidRepository;
use App\Repositories\AutoBidRepositoryInterface;
use App\Repositories\BidProductRepository;
use App\Repositories\BidProductRepositoryInterface;
use App\Repositories\ProductRepository;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Class & interface binding
        $this->app->singleton(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->singleton(BidProductRepositoryInterface::class, BidProductRepository::class);
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
        $this->app->singleton(AutoBidRepositoryInterface::class, AutoBidRepository::class);
    }
}
