<?php

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\AccountNotFoundException;
use App\Exceptions\InvalidPasswordException;
use App\Http\Requests\LoginRequest;
use App\Repositories\UserRepositoryInterface;

class AuthController extends Controller
{
    private $userRepository;

    public function __construct(Request $request, UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct($request);
    }

    public function login(LoginRequest $request)
    {
        $user = $this->userRepository->getByUserName($request->input('username'));

        if (!$user) {
            throw new AccountNotFoundException();
        }

        if (!Hash::check($request->input('password'), $user->password)) {
            throw new InvalidPasswordException();
        }

        $user->roles;
        $user->getAllPermissions();

        $token = $this->jwt($user);
        $user->api_token = $token;
        $user->save();

        return response()->json([
            'token' => $token,
            'user' => $user
        ]);
    }

    public function logout()
    {
        $this->request->currentUser->api_token = null;
        $this->request->currentUser->save();

        return response()->json($this->request->currentUser);
    }

    protected function jwt($user)
    {
        $payload = [
            'iss' => 'lumen-jwt', // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 * 24 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }
}
