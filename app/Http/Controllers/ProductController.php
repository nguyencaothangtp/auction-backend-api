<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\ProductGetRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $model = Product::class;
    private $productRepository;

    public function __construct(Request $request, ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
        parent::__construct($request);
    }

    public function get(ProductGetRequest $request)
    {
        $product = $this->productRepository->get($request->product_id);
        return new ProductResource($product);
    }

    public function create(CreateProductRequest $request)
    {
        $product = $this->productRepository->create($request->all());
        return new ProductResource($product);
    }
}
