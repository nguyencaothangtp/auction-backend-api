<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $model = User::class;
    private $userRepository;

    public function __construct(Request $request, UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct($request);
    }

    public function profile()
    {
        $user = $this->userRepository->get($this->request->currentUser->id);
        return new UserResource($user);
    }

    public function update(UserUpdateRequest $request)
    {
        $user = $this->userRepository->update($this->request->currentUser->id, $request->all());
        return new UserResource($user);
    }
}
