<?php

namespace App\Http\Controllers;

use App\Exceptions\BidClosedException;
use App\Exceptions\BiddedException;
use App\Exceptions\ExceedMaximumFundsException;
use App\Exceptions\InvalidBiddingAmountException;
use App\Http\Requests\AutoBidRequest;
use App\Http\Requests\CreateBidRequest;
use App\Http\Requests\ProductGetRequest;
use App\Http\Resources\BidProductCollection;
use App\Http\Resources\BidProductResource;
use App\Jobs\ProcessAutoBidding;
use App\Models\BidProduct;
use App\Repositories\AutoBidRepositoryInterface;
use App\Repositories\BidProductRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class BiddingController extends Controller
{
    private $bidProductRepository;
    private $userRepository;
    private $autoBidRepository;

    public function __construct(
        Request $request,
        BidProductRepositoryInterface $bidProductRepository,
        AutoBidRepositoryInterface $autoBidRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->bidProductRepository = $bidProductRepository;
        $this->autoBidRepository = $autoBidRepository;
        $this->userRepository = $userRepository;
        parent::__construct($request);
    }

    public function bid(CreateBidRequest $request)
    {
        $data['user_id'] = $request->user_id;
        $data['bid'] = $request->amount;
        $data['product_id'] = $request->product_id;

        // Get the latest bidding by current user
        $oldBid = $this->bidProductRepository->getLatestBidOfUser($data['product_id'], $data['user_id']);
        $oldBid = isset($oldBid) ? $oldBid->bid : 0;

        // Calculate new current funds value
        $user = $this->userRepository->get($data['user_id']);
        $user->current_bid_amount = $user->current_bid_amount - $oldBid + $data['bid'];
        if ($user->current_bid_amount > $user->maximum_bid_amount) {
            throw new ExceedMaximumFundsException();
        }

        $latestBid = $this->bidProductRepository->getLatestBid($data['product_id']);

        // Check if latest bid is still available to received new bidding (Ongoing status)
        if ($latestBid && $latestBid->status !== BidProduct::STATUS_ON_GOING) {
            throw new BidClosedException();
        }

        if ($latestBid && $latestBid->user_id == $data['user_id']) {
            throw new BiddedException();
        }

        if ($latestBid && $latestBid->bid >= $data['bid']) {
            throw new InvalidBiddingAmountException();
        }

        $bidProduct = $this->bidProductRepository->create($data);

        // Update user's funds
        $user->save();

        // Send message to the queue to process auto bidding
        ProcessAutoBidding::dispatch($data['product_id'], $data['bid'], $data['user_id']);

        return new BidProductResource($bidProduct);
    }

    public function turnOnAutoBid(AutoBidRequest $request)
    {
        return $this->autoBidRepository->update($request->product_id, $this->request->currentUser->id, 'on');
    }

    public function turnOffAutoBid(AutoBidRequest $request)
    {
        $this->autoBidRepository->update($request->product_id, $this->request->currentUser->id, 'off');
        return response()->json(['data' => [
            'status' => true
        ]]);
    }

    public function getAutoConfig(ProductGetRequest $request)
    {
        $config = $this->autoBidRepository->getAutoBidConfig($request->product_id, $this->request->currentUser->id);
        return response()->json(['data' => [
            'status' => !!$config
        ]]);
    }

    public function getHistory(ProductGetRequest $request)
    {
        $history = $this->bidProductRepository->getHistory($request->product_id);
        return new BidProductCollection($history);
    }
}
