<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class CreateBidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'amount' => 'required|numeric|min:0',
            'user_id' => 'required'
        ];
    }

    /**
     * Add parameters to be validated
     *
     * @param null $key
     * @return array
     */
    public function all($key = NULL)
    {
        $currentUser = app()->get('request')->currentUser;

        return array_replace_recursive(
            $this->request->all(),
            $this->route()->parameters(),
            ['user_id' => $currentUser->id]
        );
    }
}
