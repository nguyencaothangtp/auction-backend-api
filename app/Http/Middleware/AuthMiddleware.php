<?php

namespace App\Http\Middleware;

use App\Exceptions\SystemException;
use App\Exceptions\TokenExpiredException;
use App\Exceptions\TokenNotFoundException;
use App\Models\User;
use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class AuthMiddleware
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $token = str_replace('Bearer ', '', $token);

        if (!$token) {
            // Unauthorized response if token not there
            throw new TokenNotFoundException();
        }

        $user = User::where('api_token', $token)->first();

        if (!$user) {
            throw new TokenNotFoundException();
        }

        try {
            JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        }
        catch (ExpiredException $e) {
            throw new TokenExpiredException();
        }
        catch (\Exception $e) {
            throw new SystemException();
        }

        $request->currentUser = $user;

        return $next($request);
    }
}
