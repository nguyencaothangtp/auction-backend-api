<?php

namespace App\Jobs;

use App\Http\Controllers\BiddingController;
use App\Http\Requests\CreateBidRequest;
use App\Models\AutoBidConfig;
use App\Models\BidProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessAutoBidding implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $amount;
    private $userId;
    private $productId;

    /**
     * Create a new job instance.
     *
     * @param $productId
     * @param $amount
     * @param $userId
     */
    public function __construct($productId, $amount, $userId)
    {
        $this->amount = $amount;
        $this->userId = $userId;
        $this->productId = $productId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Check if user turn on auto bidding for the current product
        $autoBid = AutoBidConfig::where(['product_id' => $this->productId, 'user_id' => $this->userId])->first();
        if (!$autoBid) {
            return;
        }

        // Get from bid repo

        $autoBidUsers = AutoBidConfig::where('product_id', $this->productId)->get();
        foreach ($autoBidUsers as $userBid) {
            // Check if the latest bid is from current evaluating user. If so, skip
            $latestBid = BidProduct::where('product_id', $this->productId)
                ->orderByDesc('created_at')
                ->first();

            // Call controller
            $request = new CreateBidRequest();
            $request->product_id = $this->productId;
            $request->amount = $latestBid->bid+1;
            $request->user_id = $userBid->user_id;

            try {
                $biddingController = new BiddingController($request);
                $biddingController->bid($request);
            } catch (\Exception $e) {
                continue;
            }
        }
    }
}
