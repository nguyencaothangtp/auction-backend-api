<?php

namespace App\Repositories;

interface UserRepositoryInterface
{
    public function get($id);
    public function getByUserName($username);
    public function update($id, $data);
}
