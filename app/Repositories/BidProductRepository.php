<?php

namespace App\Repositories;

use App\Models\AutoBidConfig;
use App\Models\BidProduct;

class BidProductRepository implements BidProductRepositoryInterface
{
    public function getLatestBidOfUser($productId, $userId)
    {
        return BidProduct::where(['product_id' => $productId, 'user_id' => $userId])
            ->orderByDesc('created_at')
            ->first();
    }

    public function getLatestBid($productId)
    {
        return BidProduct::where('product_id', $productId)
            ->orderByDesc('created_at')
            ->first();
    }

    public function create($data)
    {
        return BidProduct::create($data);
    }

    public function getHistory($productId)
    {
        return BidProduct::where([
            'product_id' => $productId,
        ])->with('user')->orderByDesc('updated_at')->get();
    }
}
