<?php

namespace App\Repositories;

use App\Models\AutoBidConfig;

class AutoBidRepository implements AutoBidRepositoryInterface
{
    const STATUS_ON = 'on';
    const STATUS_OFF = 'off';

    public function update($productId, $userId, $status)
    {
        $config = null;
        switch ($status) {
            case self::STATUS_ON:
                $config = AutoBidConfig::firstOrCreate([
                    'product_id' => $productId,
                    'user_id' => $userId
                ]);
                break;
            case self::STATUS_OFF:
                $config = AutoBidConfig::where([
                    'product_id' => $productId,
                    'user_id' => $userId
                ])->first();

                if ($config) {
                    $config->delete();
                }
                break;
        }

        return $config;
    }

    public function getAutoBidConfig($productId, $userId)
    {
        return AutoBidConfig::where([
            'product_id' => $productId,
            'user_id' => $userId
        ])->first();
    }
}
