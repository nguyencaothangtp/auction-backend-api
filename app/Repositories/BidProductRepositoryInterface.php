<?php

namespace App\Repositories;

interface BidProductRepositoryInterface
{
    public function getLatestBidOfUser($productId, $userId);
    public function getLatestBid($productId);
    public function create($data);
    public function getHistory($productId);
}
