<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository implements ProductRepositoryInterface
{
    public function get($id)
    {
        return Product::find($id);
    }

    public function create($data)
    {
        return Product::create($data);
    }
}
