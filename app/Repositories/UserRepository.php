<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    public function get($id)
    {
        $user = User::find($id);
        $user->roles;
        $user->getAllPermissions();

        return $user;
    }

    public function getByUserName($username)
    {
        return User::active()->where('username', $username)->first();
    }

    public function update($id, $data)
    {
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        $user = User::find($id);
        $user->update($data);
        $user->getAllPermissions();

        return $user;
    }
}
