<?php

namespace App\Repositories;

interface ProductRepositoryInterface
{
    public function get($id);
    public function create($data);
}
