<?php

namespace App\Repositories;

interface AutoBidRepositoryInterface
{
    public function update($productId, $userId, $status);
    public function getAutoBidConfig($productId, $userId);
}
