<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    const STATUS_OPEN = 'Open';
    const STATUS_CLOSED = 'Closed';

    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'description', 'img_url', 'initial_price', 'status'];
}
