<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BidProduct extends Model
{
    const STATUS_ON_GOING = 'OnGoing';
    const STATUS_WON = 'Won';
    const STATUS_LOST = 'Lost';

    protected $fillable = ['product_id', 'user_id', 'bid', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
