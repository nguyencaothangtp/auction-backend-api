# Auction App API #

## Prerequisites

* [Docker](https://docs.docker.com/install/) ^18.09.2
* [Docker Compose](https://docs.docker.com/compose/install/) ^1.23.2

## Getting started

1. Make sure that `start.sh` file has the permission to execute:
    ```
     chmod +x start.sh
    ```
2. Start the `start.sh` script:
    ```
    ./start.sh
    ```
3. That's it!

All migrations and database seeder will run automatically.

The application runs as an HTTP server at port 90

    | Endpoint                                   | Method     |   Notes                                                     |
    | -------------------------------------------| ---------- | ------------------------------------------------------------|
    | api/v1/login                               | POST       | Login user, return JWT                                      |
    | api/v1/logout                              | GET        | Logout user                                                 |
    | api/v1/profile                             | GET        | Get current user's profile                                  |
    | api/v1/profile                             | PUT        | Update current user's profile                               |
    | api/v1/products                            | GET        | Get product list.Support pagination, order, filter by fields|
    | api/v1/products/:id                        | GET        | Get a single product detail                                 |
    | api/v1/products                            | POST       | Create new product (for admin)                              |
    | api/v1/bidding/:productId/history          | GET        | Get bidding history of a product                            |
    | api/v1/bidding/:productId/config           | GET        | Get auto bidding config of a product (for current user)     |
    | api/v1/bidding/:productId/turn-auto-bid-on | GET        | Turn on auto bidding config of a product (for current user) |
    | api/v1/bidding/:productId/turn-auto-bid-off| GET        | Turn off auto bidding config of a product (for current user)|
    | api/v1/bidding/:productId                  | POST       | Submit a bid                                                |

Here is the Postman collection, can be imported in to Postman:
https://www.getpostman.com/collections/13faaa32f5948fd1d020
