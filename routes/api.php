<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BiddingController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', [AuthController::class, 'login']);

    Route::group(['middleware' => ['auth.jwt']], function () {
        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('profile', [UserController::class, 'profile']);
        Route::put('profile', [UserController::class, 'update']);

        Route::group(['middleware' => ['auth.jwt', 'permission:create products']], function () {
            Route::post('products', [ProductController::class, 'create']);
        });

        Route::post('products', [ProductController::class, 'list']);
        Route::get('products', [ProductController::class, 'list']);
        Route::get('products/{product_id}', [ProductController::class, 'get']);

        Route::post('bidding/{product_id}', [BiddingController::class, 'bid']);
        Route::get('bidding/{product_id}/turn-auto-bid-on', [BiddingController::class, 'turnOnAutoBid']);
        Route::get('bidding/{product_id}/turn-auto-bid-off', [BiddingController::class, 'turnOffAutoBid']);
        Route::get('bidding/{product_id}/config', [BiddingController::class, 'getAutoConfig']);
        Route::get('bidding/{product_id}/history', [BiddingController::class, 'getHistory']);
    });

});
